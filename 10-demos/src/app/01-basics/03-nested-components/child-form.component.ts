import { Component, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { IPerson } from './parent.component';

@Component({
  selector: 'aw-child-form',
  templateUrl: './child-form.component.html',
  styleUrls: ['./child-form.component.scss']
})
export class ChildFormComponent {
  firstName = 'Tyler';
  lastName = 'Durden';
  age: number | undefined = 42;

  addCount = 0;

  @Output() addPerson = new EventEmitter<IPerson>();

  onAddPerson(): void {
    if (this.firstName && this.lastName && this.age) {
      const newPerson: IPerson = {
        firstName: this.firstName,
        lastName: this.lastName,
        age: this.age
      };
      this.addPerson.emit(newPerson);
      this.addCount++;
    }
  }

  reset(): void {
    this.addCount = 0;
  }
}
