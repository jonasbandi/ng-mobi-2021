import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'aw-routed',
  template: `
    <h1>Routed Component</h1>
    <h1>Current Detail: {{ detailNo }}</h1>

    <a [routerLink]="['../../other-snapshot', (detailNo ? detailNo : 0) + 1]">
      Go to other component
    </a>
    <br />
    <button (click)="goToNextDetail()">Go to next detail ...</button>
  `
})
export class RoutedComponent implements OnInit {
  detailNo: number | undefined;

  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    const detailNumberValue = this.route.snapshot.paramMap.get('detailNumber');
    if (detailNumberValue) {
      this.detailNo = parseInt(detailNumberValue, 10);
    }
  }

  goToNextDetail(): void {
    if (this.detailNo) {
      const nextNumber = this.detailNo + 1;

      // Navigate with relative link
      this.router.navigate(['../', nextNumber], { relativeTo: this.route });
    }
  }
}
