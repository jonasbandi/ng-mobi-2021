import { Component, OnDestroy } from '@angular/core';
import { BehaviorSubject, interval, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  template: `
    <h3>A parent component</h3>

    <aw-comp-child></aw-comp-child>
    <aw-comp-other></aw-comp-other>
  `
})
export class InjectionParentComponent implements OnDestroy {
  private notificationSubject = new BehaviorSubject('Initial message!');
  private intervalSubscriber: Subscription;

  notifications = this.notificationSubject.asObservable();

  constructor() {
    this.intervalSubscriber = interval(1000)
      .pipe(map(() => 'The time is: ' + new Date().toISOString()))
      .subscribe(v => this.notificationSubject.next(v));
  }

  ngOnDestroy(): void {
    this.intervalSubscriber.unsubscribe();
  }
}

// DEMO:
// Nest another <aw-comp-child></aw-comp-child> inside the <aw-comp-other></aw-comp-other>
// -> parent injection traverses up the whole component hierarchy
//
// Note: @ContentChild/@ContentChildren only selects direct children!
