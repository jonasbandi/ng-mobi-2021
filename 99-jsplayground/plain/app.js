console.log('Running example !!!');

/// API:
// http://localhost:3456/todos
// https://swapi.dev/api/people/1/

// // importing Rx
const {of, from, throwError, fromEvent, interval, merge, zip, forkJoin} = rxjs;
const {filter, map, delay, mergeMap, flatMap, switchMap, concatMap, tap, scan, take, skip} = rxjs.operators;
const {ajax} = rxjs.ajax;


function doIt(){

 const obs = interval(1000);
 //  const obs = of(42);

  // const obs = fromEvent(document.getElementById('go'), 'click');

 var observer = {
   next(v){ console.log('Value received', v)},
   error(e){ console.log('ERROR', e)},
   complete(){ console.log('Completed!')}
 };

 const subscription = obs.subscribe(observer);

 setTimeout(() => {
  subscription.unsubscribe();

 }, 5000);


}

console.log('Starting???');
doIt();
console.log('Finished???');





































// CALLBACKS
// $.get('https://swapi.co/api/people/1/', (response) => {
//   console.log('Person 1', response);
//   $.get(response.films[0], (response) => {
//     console.log('Film 1', response);
//     $.get(response.planets[0], (response) => {
//       console.log('Planet 1', response);
//     })
//   })
// });

// PROMISE
// axios.get('https://swapi.co/api/people/1/')
//   .then((response) => {
//     console.log('Person 1', response.data);
//     return axios.get(response.data.films[0])
//   })
//   .then((response) => {
//     console.log('Film 1', response.data);
//     return axios.get(response.data.planets[0])
//   })
//   .then((response) => {
//     console.log('Planet 1', response.data);
//   });

// ASYNC/AWAIT
// async function doit() {
//   const personResponse = await axios.get('https://swapi.co/api/people/1/');
//   console.log('Person 1', personResponse.data);
//
//   const filmResponse = await axios.get(personResponse.data.films[0]);
//   console.log('Film 1', filmResponse.data);
//
//   const planetResponse = await axios.get(filmResponse.data.planets[0]);
//   console.log('Planet 1', planetResponse.data);
// }
//
// doit();

// OBSERVABLES/RXJS
// ajax.get('https://swapi.co/api/people/1/')
//   .pipe(
//     tap(result => console.log('Person 1', result.response)),
//     switchMap(result => ajax.get(result.response.films[0])),
//     tap(result => console.log('Film 1', result.response)),
//     switchMap(result => ajax.get(result.response.planets[0])),
//     tap(result => console.log('Planet 1', result.response))
//   )
//   .subscribe();








