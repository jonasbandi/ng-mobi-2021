import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ToDo } from '../../model/todo.model';

@Component({
  selector: 'td-new-todo',
  templateUrl: './new-todo.component.html',
  styleUrls: ['./new-todo.component.css']
})
export class NewTodoComponent implements OnInit {

  newToDoTitle = '';

  @Output() addToDo = new EventEmitter<ToDo>();

  constructor() { }

  ngOnInit(): void {
  }

  onAddToDo(): void {
    const newToDo = new ToDo(this.newToDoTitle);
    this.addToDo.emit(newToDo);
    this.newToDoTitle = '';
  }
}
