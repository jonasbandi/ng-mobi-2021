import { Component, OnInit } from '@angular/core';
import { ToDo } from '../model/todo.model';
import { ToDoService } from '../model/todo.service';

@Component({
  templateUrl: './to-do-screen.component.html',
  providers: [ToDoService]
})
export class ToDoScreenComponent implements OnInit {
  todos: ToDo[] = [];
  doneToDos: ToDo[] = [];

  constructor(private todoService: ToDoService) {
  }

  ngOnInit(): void {
    this.loadToDos();
  }

  addToDo(newToDo: ToDo): void {
    this.todos.push(newToDo);

    console.log('Current TODOS', this.todos);

    this.todoService.saveToDos(this.todos);
  }

  completeToDo(todo: ToDo): void {
    todo.completed = true;
    this.todoService.saveToDos(this.todos);
    this.loadToDos();
    console.log('Done TODOS', this.doneToDos);
  }

  // currently not used ...
  removeToDo(todo: ToDo): void {
    this.doneToDos.splice(this.doneToDos.indexOf(todo), 1);
    this.todoService.saveDoneToDos(this.doneToDos);
  }

  private loadToDos(): void {
    const todoContainer = this.todoService.loadToDos();
    this.todos = todoContainer.todos;
    this.doneToDos = todoContainer.done;
  }
}
