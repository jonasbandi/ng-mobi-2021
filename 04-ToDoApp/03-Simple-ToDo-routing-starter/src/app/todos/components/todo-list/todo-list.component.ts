import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ToDo } from '../../model/todo.model';

@Component({
  selector: 'td-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

  @Input() todos!: ToDo[];
  @Output() removeToDo = new EventEmitter<ToDo>();

  constructor() { }

  ngOnInit(): void {
  }

  onRemoveToDo(todo: ToDo): void {
    this.removeToDo.emit(todo);
  }
}
