import { Injectable } from '@angular/core';
import { ToDo } from './todo.model';

const TODOS_KEY = 'TODO_APP.todos';

interface TodoContainer {
  todos: ToDo[];
  done: ToDo[];
}

const pendingToDosFilter = (todos: ToDo[]) => todos.filter(todo => todo.completed === false);
const doneToDosFilter = (todos: ToDo[]) => todos.filter(todo => todo.completed === true);

type JsonToDo = { _title: string; completed: boolean };

@Injectable({ providedIn: 'root' })
export class ToDoService {
  loadToDos(): TodoContainer {
    const serializedToDos = localStorage.getItem(TODOS_KEY);
    const loadedToDos: JsonToDo[] = serializedToDos ? JSON.parse(serializedToDos) : [];

    const allTodos = loadedToDos.map((json: JsonToDo) => {
      const todo = new ToDo(json._title);
      todo.completed = json.completed;
      return todo;
    });

    const todos = allTodos.filter(t => t.completed === false);
    const done = allTodos.filter(t => t.completed === true);

    return { todos, done };
  }

  saveToDos(todos: Array<ToDo>): void {
    const doneToDos = this.loadToDos().done;
    const newDoneToDos = doneToDosFilter(todos);
    const pendingTodos = pendingToDosFilter(todos);

    localStorage.setItem(TODOS_KEY, JSON.stringify([...pendingTodos, ...newDoneToDos, ...doneToDos]));
  }

  saveDoneToDos(doneToDos: ToDo[]): void {
    const pendingToDos = this.loadToDos().todos;

    localStorage.setItem(TODOS_KEY, JSON.stringify([...pendingToDos, ...doneToDos]));
  }
}
