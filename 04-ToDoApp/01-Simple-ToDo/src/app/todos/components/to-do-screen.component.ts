import { Component, OnInit } from '@angular/core';
import { ToDo } from '../model/todo.model';
import { ToDoService } from '../model/todo.service';

@Component({
  templateUrl: './to-do-screen.component.html'
})
export class ToDoScreenComponent implements OnInit {
  newToDoTitle = '';
  todos: ToDo[] = [];
  doneToDos: ToDo[] = [];

  constructor(private todoService: ToDoService) {}

  ngOnInit(): void {
    this.loadToDos();
  }

  addToDo(): void {
    this.todos.push(new ToDo(this.newToDoTitle));
    this.todoService.saveToDos(this.todos);
    this.newToDoTitle = '';
  }

  completeToDo(todo: ToDo): void {
    todo.completed = true;
    this.todoService.saveToDos(this.todos);
    this.loadToDos();
  }

  // currently not used ...
  removeToDo(todo: ToDo): void {
    this.doneToDos.splice(this.doneToDos.indexOf(todo), 1);
    this.todoService.saveDoneToDos(this.doneToDos);
  }

  private loadToDos(): void {
    const todoContainer = this.todoService.loadToDos();
    this.todos = todoContainer.todos;
    this.doneToDos = todoContainer.done;
  }
}
